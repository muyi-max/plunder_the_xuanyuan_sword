//midi.cpp

#include "stdafx.h"


//
DWORD CMidi::Play(HWND hwnd,char* MidiFile)
{
	Stop();
	mciOpenParms.lpstrDeviceType = "sequencer";
	mciOpenParms.lpstrElementName = MidiFile; //要播放的MIDI
	mciSendCommand(	0, MCI_OPEN, MCI_OPEN_ELEMENT | MCI_OPEN_TYPE, (DWORD)(LPVOID)&mciOpenParms);	// 打开
	wDeviceID=mciOpenParms.wDeviceID;
	mciSendCommand(wDeviceID,MCI_PLAY,MCI_NOTIFY,(DWORD)(LPVOID)&mciPlayParms);	//播放
	//
    mciPlayParms.dwCallback = (DWORD) hwnd;
    if (dwReturn = mciSendCommand(wDeviceID, MCI_PLAY, MCI_NOTIFY, (DWORD)(LPVOID) &mciPlayParms))
    {
        mciSendCommand(wDeviceID, MCI_CLOSE, 0, NULL);
        return (dwReturn);
    }
	//
    return (0L);
};
//
void CMidi::Replay(WPARAM w)
{
	if( wDeviceID!=0 && w==MCI_NOTIFY_SUCCESSFUL )
	{
		mciSendCommand(wDeviceID, MCI_SEEK, MCI_SEEK_TO_START, NULL);
		mciSendCommand(wDeviceID, MCI_PLAY, MCI_NOTIFY, (DWORD)(LPVOID) &mciPlayParms);
	}
};
//
void CMidi::Stop()
{
	if( wDeviceID != 0 )
	{
		mciSendCommand(wDeviceID, MCI_STOP, MCI_WAIT, NULL);
		mciSendCommand(wDeviceID, MCI_CLOSE, 0, NULL);
		wDeviceID = 0;
	}
};
//
void CMidi::PlayWav(char* FileName)
{
	sndPlaySound(FileName, SND_ASYNC|SND_NODEFAULT);
};
//the end.