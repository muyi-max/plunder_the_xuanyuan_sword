// GameMap.h: interface for the CGameMap class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(__GAMEMAP_H__)
#define __GAMEMAP_H__

#define EVENT_DELAY 10

class CHero;
class CNpc;
class CEnemy;
class CMainFrame;

class CGameMap  
{
public:
	// 主窗口
	CMainFrame* m_pMainFrm;

	// 防止事件被连续触发的延时变量
	int m_nEventDelay;
	// 定义图片设备
	HDC m_hImageDC;

#ifdef DEBUG
// 调试使用的事件编号位图
	HDC m_hNpcNumberDC;
#endif

	// NPC图像设备数组（相同类型的NPC使用同一套图片）
	HDC m_hNpcImageDC[7];
	// NPC死亡图像
	HDC m_hNpcDeadDC;
	// NPC动作数组
	char** m_psNpcAction;
	// 定义地图矩阵
	int map[GRIDE_H][GRIDE_W];
	// 定义事件矩阵
	int npc[GRIDE_H][GRIDE_W];
	// 定义地图左上角点
	int m_nLeft;
	int m_nTop;
	// Hero的指针
	CHero* m_pHero;
	// NPC数组
	int m_nNpcCount;
	CNpc** m_paNpcArray;
	// BOSS
	CBoss* m_pBoss;

public:
	CGameMap( CMainFrame* pMF );
	virtual ~CGameMap();

	// 初始化地图
	BOOL LoadMap( char* psImgFileName, char* psMapFileName, char* psNpcFileName );

	// 从字符串中取得一行
	BOOL GetLine( char* cString, char* cReturn, int k );

	// 释放地图资源
	BOOL ReleaseMap();

	// 碰撞检测（监测某物体与其他物体之间的碰撞情况）
	CGameObject* HitTest( CGameObject* pObj );

	// 碰壁检测
	BOOL HitWallTest( CGameObject* pObj );

	// 事件检测
	BOOL EventTest( CGameObject* pObj );

	// 设置地图左上角点
	BOOL SetLeftTop( int x, int y );

	// 获得地图左上角点
	BOOL GetLeftTop( int* px, int* py );

	// 移动地图
	BOOL MoveMap( int nOffset_x, int nOffset_y );

	// 精灵运动
	BOOL MoveObjects( long lNow );

	// 绘制地图
	void Draw( HDC hDC );

/* 角色运动 */
	BOOL GoUp( long lNow );
	BOOL GoDown( long lNow );
	BOOL GoLeft( long lNow );
	BOOL GoRight( long lNow );
	BOOL Fire( long lNow );
	BOOL RenewCassette( long lNow );
// 主角运动部分结束

	// 子弹飞行的碰撞检测处理
	BOOL BulletGo( int bullet_power, int* bullet_x, int* bullet_y );

	// 攻击角色
	void HitHero( int nPower );

	// 增加角色的经验值
	void AddExp( int nExp );

	// 获得角色属性
	int GetHeroLife();
	int GetHeroLevel();
	int GetHeroExp();

	// 添加新的NPC
	void AddNpc( int x, int y, int type );

	// 清除所有的NPC
	void CleanNpc();

	// 设置BOSS
	void SetBoss( int X, int Y, int event );

	// 删除BOSS
	void CleanBoss();
};

#endif // !defined(__GAMEMAP_H__)
