// Wave.h: interface for the CWave class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(__WAVE_H__)
#define __WAVE_H__

class CWave : public CObject
{
protected:
    char* m_filename;
    WAVEFORMATEX m_wfe;
    BYTE* m_pData;
    DWORD m_size;
    BOOL m_valid;

public:
	CWave(char* psFileName);
	virtual ~CWave();

    BOOL IsValid();
    DWORD GetSize();
    BYTE* GetData();
    char* GetFilename();
    LPWAVEFORMATEX GetFormat();

protected:
    BOOL LoadFile(char* psFileName);

};

#endif // !defined(__WAVE_H__)
