// macro.h

#if !defined(__MACRO_H__)
#define __MACRO_H__

// 关于屏幕尺寸、位置的宏，以SCREEN_开头
#define DIRECT_SCREEN_W          800    // 使用Direct下屏幕的宽度（仅初始化Direct用，实际尺寸会变）
#define DIRECT_SCREEN_H          600    // 使用Direct下屏幕的高度（仅初始化Direct用，实际尺寸会变）
#define SCREEN_W                 760    // 显示区域宽度
#define SCREEN_H                 480    // 显示区域高度

// 定义透明色
#define COLORKEY                 0x00ff00ff

// 重绘屏幕的时间间隙（单位毫秒）
#define RENEW_SCREEN_GAP	     30

// 行走动作切换间隔
#define STEP_DELAY               2

// 标题栏文字
#define GAME_TITLE			     "劫掠轩辕剑二代大炮版"

// 同时显示的动画数量
#define AVI_MAX                  6

// 商标尺寸位置
#define BRAND_X                  251
#define BRAND_Y                  439
#define BRAND_W                  165
#define BRAND_H                  38

// 游戏区域
#define GAME_AREA_W              608
#define GAME_AREA_H              480

// 人物头像的宽度和高度
#define FACE_W                   128
#define FACE_H                   128

// 物品图片的宽度和高度
#define ITEM_W                   256
#define ITEM_H                   128

// 状态栏的位置信息
#define STATUS_BAR_X             608
#define STATUS_BAR_Y             0
#define STATUS_BAR_W             152
#define STATUS_BAR_H             480

// 按钮的位置信息
#define BUTTON_GUN1_X            622
#define BUTTON_GUN1_Y            15
#define BUTTON_GUN1_W            129
#define BUTTON_GUN1_H            51

#define BUTTON_GUN2_X            622
#define BUTTON_GUN2_Y            90
#define BUTTON_GUN2_W            129
#define BUTTON_GUN2_H            51

#define BUTTON_GUN3_X            622
#define BUTTON_GUN3_Y            165
#define BUTTON_GUN3_W            129
#define BUTTON_GUN3_H            51

#define BUTTON_GUN4_X            622
#define BUTTON_GUN4_Y            240
#define BUTTON_GUN4_W            129
#define BUTTON_GUN4_H            51

#define BUTTON_LIFE_X            629
#define BUTTON_LIFE_Y            344
#define BUTTON_LIFE_W            114
#define BUTTON_LIFE_H            10

#define BUTTON_EXP_X             629
#define BUTTON_EXP_Y             392
#define BUTTON_EXP_W             114
#define BUTTON_EXP_H             10

#define BUTTON_NEW_X             622
#define BUTTON_NEW_Y             330
#define BUTTON_NEW_W             129
#define BUTTON_NEW_H             39

#define BUTTON_LOAD_X            622
#define BUTTON_LOAD_Y            380
#define BUTTON_LOAD_W            129
#define BUTTON_LOAD_H            39

#define BUTTON_EXIT_X            622
#define BUTTON_EXIT_Y            430
#define BUTTON_EXIT_W            129
#define BUTTON_EXIT_H            39

#define LABEL_LEVEL_UP_X         622
#define LABEL_LEVEL_UP_Y         365
#define LABEL_LEVEL_UP_W         129
#define LABEL_LEVEL_UP_H         19

#define BACK_GROUND_TILE_W       129
#define BACK_GROUND_TILE_H       90

// 对话栏出现的位置
#define DIALOG_X                 40
#define DIALOG_Y                 328
#define DIALOG_W                 528
#define DIALOG_H                 128
// 对话栏上面的Next小图标
#define BUTTON_NEXT_X            240
#define BUTTON_NEXT_Y            440
#define BUTTON_NEXT_W            128
#define BUTTON_NEXT_H            20

// 地图属性
#define TILE_W                   32
#define TILE_H                   32
#define GRIDE_W                  200
#define GRIDE_H                  200

// 角色属性
#define HERO_W                   32
#define HERO_H                   48
#define HERO_CENTER_OFFSET_X     16
#define HERO_CENTER_OFFSET_Y     32

#define HERO_ACT_REST            0x00              // 无动作
#define HERO_ACT_GO_UP           0x01              // 向上走
#define HERO_ACT_GO_DOWN         0x02              // 向下走
#define HERO_ACT_GO_LEFT         0x03              // 向左走
#define HERO_ACT_GO_RIGHT        0x04              // 向右走
#define HERO_ACT_DEAD            0x08              // 角色死亡
#define HERO_ACT_FIRE            0x10              // 开火
#define HERO_ACT_RENEW_CASSETTE  0x20              // 更换弹夹
#define HERO_ACT_GO_COMPLETE     0x40              // 行走动作执行完成标志
#define HERO_ACT_OTHER_COMPLETE  0x80              // 其他动作执行完成标志

// 副角色与角色之间的相对位置
#define HERO2_OFFSET_X           16
#define HERO2_OFFSET_Y           -16

// NPC属性
#define NPC_W                    32
#define NPC_H                    32
#define NPC_CENTER_OFFSET_X      16
#define NPC_CENTER_OFFSET_Y      16

#define NPC_STEP                 4
#define NPC_ACT_REST             0x00              // 无动作
#define NPC_ACT_GO_UP            0x01              // 向上走
#define NPC_ACT_GO_DOWN          0x02              // 向下走
#define NPC_ACT_GO_LEFT          0x03              // 向左走
#define NPC_ACT_GO_RIGHT         0x04              // 向右走
#define NPC_ACT_FIRE             0x08              // 攻击
#define NPC_ACT_BE_HIT           0x10              // 受攻击
#define NPC_ACT_DEAD             0x20              // 死亡
#define NPC_ACT_GO_COMPLETE      0x40              // 行走动作完成标志
#define NPC_ACT_OTHER_COMPLETE   0x80              // 其他动作执行完成标志

// 定义游戏的四种状态
#define GAME_ST_START            1                 // 开始画面
#define GAME_ST_DIALOG           2                 // 对话态
#define GAME_ST_WALK             3                 // 行走态
#define GAME_ST_FIGHT            4                 // 战斗态

// 定义武器代号
#define WEAPEN_GUN1              1                 // 冲锋枪
#define WEAPEN_GUN2              2                 // 自动步枪
#define WEAPEN_GUN3              3                 // 轻机枪
#define WEAPEN_GUN4              4                 // 无后坐力炮

#define WEAPEN_ICON_LEFT         414               // 武器图标的左边界
#define WEAPEN_ICON_TOP_PISTOL   30
#define WEAPEN_ICON_TOP_GUN1     82
#define WEAPEN_ICON_TOP_GUN2     134
#define WEAPEN_ICON_TOP_GUN3     186
#define WEAPEN_ICON_W            204               // 武器图标的宽度
#define WEAPEN_ICON_H            50                // 武器图标的高度

#define STATUS_AREA_LEFT         409               // 状态栏左边
#define STATUS_AREA_TOP          23                // 状态栏顶边
#define STATUS_AREA_W            214               // 状态栏宽度
#define STATUS_AREA_H            221               // 状态栏高度

#define STATUS_WEAPEN            1                 // 状态栏显示武器状态
#define STATUS_MAP               2                 // 状态栏显示小地图

#define WALK_STEP                16                // 行走时的步长

#define ID_BUTTON_NEW_GAME       120001
#define ID_BUTTON_LOAD_GAME      120002
#define ID_BUTTON_QUIT           120003
#define ID_BUTTON_INTRO_1        120004
#define ID_BUTTON_INTRO_2        120005
#define ID_BUTTON_INTRO_3        120006
#define ID_LIST_INTRO            120007

#define ID_DLG_GAME_WINDOW       102
#define ID_DLG_LOAD_GAME         103
#define ID_DLG_SAVE_GAME         104

#define LIST_COUNT_INTRO_1       29
#define LIST_COUNT_INTRO_2       17
#define LIST_COUNT_INTRO_3       30

// 炮弹爆炸图片单元的宽高
#define BOMB_IMG_W               160
#define BOMB_IMG_H               160

// 定义鼠标光标图片的尺寸
#define CURSOR_W                     88
#define CURSOR_H                     75
#define FILENAME_IMG_CURSOR          "Img/cursor.bmp"

// 定义各种文件名
#define FILENAME_WAV_START           "Snd/start.wav"
#define FILENAME_WAV_KEYPRESS        "Snd/keypress.wav"
#define FILENAME_WAV_SHOOT1          "Snd/shoot1.wav"
#define FILENAME_WAV_SHOOT2          "Snd/shoot2.wav"
#define FILENAME_WAV_SHOOT3          "Snd/shoot3.wav"
#define FILENAME_WAV_SHOOT4          "Snd/shoot4.wav"
#define FILENAME_WAV_ENEMY_SHOOT     "Snd/enemyshoot.wav"
#define FILENAME_WAV_ENEMY_DEAD      "Snd/enemydead.wav"
#define FILENAME_WAV_RENEW_CASSETTE  "Snd/renewcassette.wav"
#define FILENAME_WAV_CHANGE_GUN      "Snd/changegun.wav"
#define FILENAME_WAV_LEVEL_UP        "Snd/levelup.wav"
#define FILENAME_WAV_DEAD            "Snd/dead.wav"
#define FILENAME_IMG_TITLE           "Img/title.bmp"
#define FILENAME_IMG_BAR             "Img/bar.bmp"
#define FILENAME_IMG_BUTTONS         "Img/buttons.bmp"
#define FILENAME_IMG_BOMB            "Img/bomb.bmp"
#define FILENAME_IMG_RAINBOW         "Img/rainbow.bmp"
#define FILENAME_NPC_ACTION          "Map/actions.txt"

#define FILENAME_MIDI_END            "Snd/end.mid"
#define FILENAME_MIDI_DEAD           "Snd/dead.mid"

#define PREFIX_NPC_IMAGE             "Img/ENEMY0"
#define SUFFIX_NPC_IMAGE             ".bmp"
#define FILENAME_NPC_DEAD            "Img/npcdead.bmp"
#define FILENAME_IMG_BOSS            "Img/boss.bmp"
#define FILENAME_WAV_BOSS_KILL       "Snd/circle.wav"

#define FILENAME_GAME_SAVE           "xyj.sav" //改成保存在exe所在目录下

static char DSOUND_FAILURE[] = {"Create DirectSound Failed!\n初始化DirectSound过程中出错！\
			或者有其他设备正占用声卡！程序将采用系统默认方法播放音效，\
            您将无法听到混合音效声音。"};

static char DDRAW_FAILURE[] = {"Direct Draw系统初始化失败，\
            \n程序将使用GDI函数显示图像，\
            \n图像的显示速度可能会因此受到影响。\
            \n如果将桌面调整为16位色，可提高显示速度。"};

#endif // !defined(__MACRO_H__)