void CMainFrame::Para2Init(void)
{
/////////////////////////////////////////////////////////////////////////////
//调入所需对话，主角图片，地图文件群。
	LoadDialog("dlg\\para2.dlg");
	LoadActor("pic\\hero.bmp");//调入主角
	OpenMap("map\\maps106.bmp","map\\map106a.map","map\\map106a.npc");//调入地图
	deleteallbmpobjects();
/////////////////////////////////////////////////////////////////////////////
//调入所需头像
	hbmp_Photo0=(HBITMAP)LoadImage(NULL,"pic\\pic321.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//洛桑镇长
	hbmp_Photo1=(HBITMAP)LoadImage(NULL,"pic\\pic322.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//西蒙镇长
	hbmp_Photo2=(HBITMAP)LoadImage(NULL,"pic\\pic309.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//医生哈德尔
	hbmp_Photo3=(HBITMAP)LoadImage(NULL,"pic\\pic320.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//德里安
	hbmp_Photo4=(HBITMAP)LoadImage(NULL,"pic\\pic315.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//荻娜（1）
	hbmp_Photo5=(HBITMAP)LoadImage(NULL,"pic\\pic316.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//荻娜（2）
	hbmp_Photo6=(HBITMAP)LoadImage(NULL,"pic\\pic310.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺（1）
	hbmp_Photo7=(HBITMAP)LoadImage(NULL,"pic\\pic312.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺（2）
	hbmp_Photo8=(HBITMAP)LoadImage(NULL,"pic\\pic313.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺（3）
/////////////////////////////////////////////////////////////////////////////
//调入所需道具
	hbmp_Prop0=(HBITMAP)LoadImage(NULL,"pic\\pic202.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//信
	hbmp_Prop1=(HBITMAP)LoadImage(NULL,"pic\\pic205.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//洋地黄花
	hbmp_Prop2=(HBITMAP)LoadImage(NULL,"pic\\pic208.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//地图
/////////////////////////////////////////////////////////////////////////////
//调入其他图片
	hbmp_0=(HBITMAP)LoadImage(NULL,"pic\\pic107.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//荻娜
	hbmp_1=(HBITMAP)LoadImage(NULL,"pic\\pic108.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//老头
	hbmp_2=(HBITMAP)LoadImage(NULL,"pic\\pic109.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//哈德尔
	hbmp_3=(HBITMAP)LoadImage(NULL,"pic\\hero.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺
/////////////////////////////////////////////////////////////////////////////
//引入开始游戏的位置
	if(st_sub1==0){
		m_info_st=9;//对话态
		m_oldinfo_st=9;
	}
/////////////////////////////////////////////////////////////////////////////
}
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para2Info(int type)
{
	int i;

if(st_sub1==0){
	if(st_dialog==0)
	{
		Dialog(4,0,0,0,1);
		st_dialog=1;
	}
	else if(st_dialog==1)
	{
		CDC *pdc=GetDC();
		PlayMidi("midi\\p1007.mid");
		current.x=60;
		current.y=153;
		map[86][134]=633;//在地图上添加卡诺和老师
		map[87][134]=634;
		map[90][134]=631;
		map[91][134]=632;
//		map[86][144]=222;//恢复修改所用的代码
//		map[87][144]=223;
//		map[90][144]=222;
//		map[91][144]=223;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		vopen();
		current.x+=1;
		for(i=0;i<30;i++){//镜头向右移动31格
			movest=13;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(60);
			movest=14;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(60);
			movest=15;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(60);
			movest=16;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(60);
			current.x+=1;
		}
		for(i=0;i<14;i++){////镜头向上移动14格
			movest=1;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(60);
			movest=2;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(60);
			movest=3;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(60);
			movest=4;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(60);
			current.y-=1;
		}
		movest=0;
		GameSleep(1000);
		DrawMap();//绘制地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
		GameSleep(300);
		map[86][134]=635;//替换地图以实现卡诺眨眼
		map[87][134]=636;
		DrawMap();
		pdc->BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		GameSleep(300);
		map[86][134]=633;//替换地图以实现卡诺眨眼
		map[87][134]=634;
		DrawMap();
		pdc->BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		GameSleep(300);
		map[86][134]=635;//替换地图以实现卡诺眨眼
		map[87][134]=636;
		DrawMap();
		pdc->BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		GameSleep(300);
		map[86][134]=633;//替换地图以实现卡诺眨眼
		map[87][134]=634;
		DrawMap();
		pdc->BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		GameSleep(300);
		map[86][134]=635;//替换地图以实现卡诺眨眼
		map[87][134]=636;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
		GameSleep(500);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,36,0,2);
		st_dialog=2;
	}//以上部分已经通过了测试
	else if(st_dialog==2)
	{//镇长走过来
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_1);//老头
		resetblt();
		for(i=0;i<32;i++){//移动
			blt((808-(i*16)),368,32,48,160,48,64,48,pdc);
			GameSleep(50);
			blt((800-(i*16)),368,32,48,128,48,32,48,pdc);
			GameSleep(50);
		}
		for(i=0;i<16;i++){//移动
			blt(288,(368-(i*16)),32,48,160,96,64,96,pdc);
			GameSleep(50);
			blt(288,(360-(i*16)),32,48,96,96,32,96,pdc);
			GameSleep(50);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,1,0,3);
		st_dialog=3;
	}
	else if(st_dialog==3)
	{//卡诺下床
		CDC *pdc=GetDC();
		map[86][134]=222;//恢复床铺
		map[87][134]=223;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		//将卡诺绘制在床边
		BuffDC.BitBlt(288,64,32,48,&ActorDC,96,0,SRCAND);//卡诺
		BuffDC.BitBlt(288,64,32,48,&ActorDC,0,0,SRCINVERT);
		MemDC.SelectObject(hbmp_1);//老头
		resetblt();
		blt(288,120,32,48,96,96,32,96,pdc);
		pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,36,0,4);
		st_dialog=4;
	}
	else if(st_dialog==4)
	{
		Dialog(4,0,1,0,5);
		st_dialog=5;
	}
	else if(st_dialog==5)
	{
		Dialog(4,0,31,0,6);
		st_dialog=6;
	}
	else if(st_dialog==6)
	{//卡诺走到卧虫跟前
		CDC *pdc=GetDC();
		BuffDC.BitBlt(288,64,64,48,&MapDC,288,88,SRCCOPY);
		BuffDC.BitBlt(320,64,32,48,&ActorDC,96,144,SRCAND);//卡诺
		BuffDC.BitBlt(320,64,32,48,&ActorDC,0,144,SRCINVERT);
		pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,0,0,7);
		st_dialog=7;
	}
	else if(st_dialog==7)
	{
		Dialog(4,0,31,0,8);
		st_dialog=8;
	}
	else if(st_dialog==8)
	{
		Dialog(4,0,0,0,9);
		st_dialog=9;
	}
	else if(st_dialog==9)
	{
		Dialog(2,0,0,0,10);
		st_dialog=10;
	}
	else if(st_dialog==10)
	{
		Dialog(4,0,1,0,11);
		st_dialog=11;
	}
	else if(st_dialog==11)
	{//镇长招呼卡诺走开
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_1);//老头
		int b=72;
		BuffDC.BitBlt(288,64,128,128,&MapDC,288,64,SRCCOPY);
		pdc->BitBlt(288,64,128,128,&BuffDC,288,64,SRCCOPY);
		resetblt();
		for(i=0;i<14;i++){//移动
			BuffDC.BitBlt(288,(b-16),32,64,&MapDC,288,(b-16),SRCCOPY);
			BuffDC.BitBlt(288,b,32,48,&ActorDC,128,0,SRCAND);//卡诺
			BuffDC.BitBlt(288,b,32,48,&ActorDC,32,0,SRCINVERT);
			pdc->BitBlt(288,(b-16),32,64,&BuffDC,288,(b-16),SRCCOPY);
			blt(288,(b+64),32,48,160,0,64,0,pdc);
			b+=8;
			GameSleep(80);
			BuffDC.BitBlt(288,(b-16),32,64,&MapDC,288,(b-16),SRCCOPY);
			BuffDC.BitBlt(288,b,32,48,&ActorDC,160,0,SRCAND);//卡诺
			BuffDC.BitBlt(288,b,32,48,&ActorDC,64,0,SRCINVERT);
			pdc->BitBlt(288,(b-16),32,64,&BuffDC,288,(b-16),SRCCOPY);
			blt(288,(b+64),32,48,128,0,32,0,pdc);
			b+=8;
			GameSleep(80);
		}
		BuffDC.BitBlt(288,(b-24),32,64,&MapDC,288,(b-24),SRCCOPY);
		BuffDC.BitBlt(288,b,32,48,&ActorDC,96,0,SRCAND);//卡诺
		BuffDC.BitBlt(288,b,32,48,&ActorDC,0,0,SRCINVERT);
		pdc->BitBlt(288,(b-24),32,72,&BuffDC,288,(b-24),SRCCOPY);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,1,0,12);
		st_dialog=12;
	}
	else if(st_dialog==12)
	{
		Dialog(4,0,31,0,13);
		st_dialog=13;
	}
	else if(st_dialog==13)
	{
		Dialog(4,0,1,0,14);
		st_dialog=14;
	}
	else if(st_dialog==14)
	{//村长走，初始化自由移动环境
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_1);//老头
		resetblt();
		for(i=0;i<32;i++){//移动
			blt((288+(i*16)),352,32,48,160,144,64,144,pdc);
			GameSleep(80);
			blt((296+(i*16)),352,32,48,128,144,32,144,pdc);
			GameSleep(80);
		}
		vclose();
		ReleaseDC(pdc);
		pdc = NULL;
		//PlayMidi("midi\\p1008.mid");
		Dialog(2,0,0,0,142);
		st_dialog=15;
	}
	else if(st_dialog==15)
	{
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		azimuth= AZIMUTH_LEFT;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		vopen();
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
		st_progress=0;
	}//以上部分已经通过测试。这是进入行走状态前的所有代码。
////////////////////////////////////////////////////////////////////////////////
	else if(st_dialog==20)
	{//不论跟谁说话，最后的st_dialog都要变成20，以落入此处，方能恢复走动。
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
	}
////////////////////////////////////////////////////////////////////////////////
	else if(st_dialog==21)
	{//向大港镇的村民打听去哈德尔家的道路
		Dialog(4,0,0,0,23);
		st_dialog=20;
	}
////////////////////////////////////////////////////////////////////////////////
	else if(st_dialog==30)//以下部分是跟荻娜的对话
	{
		Dialog(4,21,0,0,25);
		st_dialog=31;
	}
	else if(st_dialog==31)
	{
		Dialog(4,0,31,0,26);
		st_dialog=32;
	}
	else if(st_dialog==32)
	{
		Dialog(4,21,0,0,27);
		st_dialog=33;
	}
	else if(st_dialog==33)
	{
		Dialog(4,0,31,0,28);
		st_dialog=34;
	}
	else if(st_dialog==34)
	{
		Dialog(4,21,0,0,29);
		st_dialog=35;
	}
	else if(st_dialog==35)
	{
		Dialog(4,0,31,0,30);
		st_dialog=36;
	}
	else if(st_dialog==36)
	{
		Dialog(4,21,0,0,31);
		st_dialog=37;
	}
	else if(st_dialog==37)
	{
		Dialog(4,0,31,0,32);
		st_dialog=38;
	}
	else if(st_dialog==38)
	{//哈德尔匆匆进来
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_2);//哈德尔
		resetblt();
		for(i=0;i<26;i++){//移动
			blt((808-(i*16)),304,32,48,160,48,64,48,pdc);
			GameSleep(50);
			blt((800-(i*16)),304,32,48,128,48,32,48,pdc);
			GameSleep(50);
		}
		blt(384,304,32,48,96,96,0,96,pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,11,0,33);
		st_dialog=40;
	}
	else if(st_dialog==40)
	{//卡诺应回过头
		CDC *pdc=GetDC();
		BuffDC.BitBlt(384,208,32,48,&MapDC,384,208,SRCCOPY);//背景
		BuffDC.BitBlt(384,208,32,48,&ActorDC,96,0,SRCAND);//卡诺
		BuffDC.BitBlt(384,208,32,48,&ActorDC,0,0,SRCINVERT);
		pdc->BitBlt(384,208,32,48,&BuffDC,384,208,SRCCOPY);//显示
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,31,0,34);
		st_dialog=41;
	}
	else if(st_dialog==41)
	{
		Dialog(4,0,11,0,35);
		st_dialog=42;
	}
	else if(st_dialog==42)
	{
		Dialog(4,21,0,0,36);
		st_dialog=43;
	}
	else if(st_dialog==43)
	{//荻娜向外走，哈德尔叫住荻娜
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_0);//荻娜
		BuffDC.BitBlt(320,96,32,64,&MapDC,288,96,SRCCOPY);//在背景上擦去荻娜
		pdc->BitBlt(320,96,32,64,&BuffDC,288,96,SRCCOPY);//显示
		resetblt();
		for(i=0;i<2;i++){//移动
			blt((352+(i*16)),112,32,48,160,144,64,144,pdc);
			GameSleep(50);
			blt((360+(i*16)),112,32,48,128,144,32,144,pdc);
			GameSleep(100);
		}
		for(i=0;i<3;i++){//移动
			blt(384,(112+(i*16)),32,48,160,0,64,0,pdc);
			GameSleep(50);
			blt(384,(120+(i*16)),32,48,128,0,32,0,pdc);
			GameSleep(100);
		}
		for(i=0;i<4;i++){//移动
			blt(416,(176+(i*16)),32,48,160,0,64,0,pdc);
			GameSleep(50);
			blt(416,(186+(i*16)),32,48,128,0,32,0,pdc);
			GameSleep(100);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,11,0,37);
		st_dialog=44;
	}
	else if(st_dialog==44)
	{
		Dialog(4,22,0,0,38);
		st_dialog=45;
	}
	else if(st_dialog==45)
	{
		Dialog(4,0,31,0,39);
		st_dialog=46;
	}
	else if(st_dialog==46)
	{
		Dialog(4,0,11,0,40);
		st_dialog=47;
	}
	else if(st_dialog==47)
	{
		Dialog(4,22,0,0,41);
		st_dialog=48;
	}
	else if(st_dialog==48)
	{
		CDC *pdc=GetDC();
		blt(0,0,0,0,0,0,0,0,pdc);//擦去荻娜（荻娜加入队伍）
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,22,0,0,42);
		lclose();
		st_dialog=49;
	}
	else if(st_dialog==49)
	{//提示存盘。
		st_dialog=50;
		Dialog(5,0,0,0,137);//参数5表示存盘
	}
	else if(st_dialog==50)
	{//上山作战，启用新的地图
		st_sub1=1;//第二阶段
		st_dialog=0;
		Dialog(2,0,0,0,143);
	}
}//第一部分结束
////////////////////////////////////////////////////////////////////////////////
else if(st_sub1==1){//第二部分开始
	if(st_dialog==0)
	{
		PlayMidi("midi\\p1006.mid");
		CDC *pdc=GetDC();
		RenewInfo(pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		current.x=66;
		current.y=95;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		azimuth= AZIMUTH_RIGHT;
		movest=0;
		OpenMap("map\\maps103.bmp","map\\map103b.map","map\\map103a.npc");//调入地图
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		lopen();

		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
	}
	else if(st_dialog==20)
	{//不论跟谁说话，最后的st_dialog都要变成20，以落入此处，方能恢复走动。
		m_info_st=m_oldinfo_st;//恢复原状态
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
	}
	else if(st_dialog==30)
	{//采到药以后的对话
		CDC *pdc=GetDC();
		m_info_prop1=0;
		RenewInfo(pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,11,0,0,48);
		st_dialog=31;
	}
	else if(st_dialog==31)
	{//切换到西镇村长家
		vclose();
		OpenMap("map\\maps106.bmp","map\\map106a.map","map\\map106a.npc");//调入地图
		map[90][134]=631;//在地图上添加卧虫
		map[91][134]=632;
		map[91][137]=754;//在地图上添加村长
		map[91][138]=794;
		current.x=91;
		current.y=139;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_2);//哈德尔
		BuffDC.BitBlt(416,64,32,48,&MemDC,96,48,SRCAND);//添加哈德尔
		BuffDC.BitBlt(416,64,32,48,&MemDC,0,48,SRCINVERT);
		BuffDC.BitBlt(320,64,32,48,&ActorDC,96,144,SRCAND);//添加卡诺
		BuffDC.BitBlt(320,64,32,48,&ActorDC,0,144,SRCINVERT);
		vopen();
		GameSleep(2000);
		Dialog(4,0,1,0,50);
		st_dialog=32;
	}
	else if(st_dialog==32)
	{
		Dialog(4,0,31,0,51);
		st_dialog=33;
	}
	else if(st_dialog==33)
	{
		Dialog(4,11,0,0,52);
		st_dialog=34;
	}
	else if(st_dialog==34)
	{
		Dialog(4,0,1,0,53);
		st_dialog=35;
	}
	else if(st_dialog==35)
	{
		Dialog(4,0,31,0,56);
		st_dialog=36;
	}
	else if(st_dialog==36)
	{
		vclose();
		st_dialog=50;
		Dialog(5,0,0,0,137);//提示存盘
	}
	else if(st_dialog==50)
	{
		st_sub1=2;//第三阶段
		st_dialog=0;
		Dialog(2,0,0,0,57);
	}
}//第二部分结束
////////////////////////////////////////////////////////////////////////////////
else if(st_sub1==2){//第三部分开始
	if(st_dialog==0)
	{
		PlayMidi("midi\\p1010.mid");
		OpenMap("map\\maps106.bmp","map\\map106a.map","map\\map106a.npc");//调入地图
		current.x=106;
		current.y=138;
		azimuth= AZIMUTH_UP;
		movest=0;
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		vopen();
		Dialog(4,0,1,0,58);
		st_dialog=1;
	}
	else if(st_dialog==1)
	{
		Dialog(4,0,31,0,59);
		st_dialog=2;
	}
	else if(st_dialog==2)
	{
		Dialog(4,0,1,0,60);
		st_dialog=3;
	}
	else if(st_dialog==3)
	{
		Dialog(4,0,31,0,61);
		st_dialog=4;
	}
	else if(st_dialog==4)
	{
		Dialog(4,0,1,0,62);
		st_dialog=5;
	}
	else if(st_dialog==5)
	{
		Dialog(4,0,31,0,63);
		st_dialog=10;
	}
	else if(st_dialog==10)
	{//进入自由行走，任务是去找大港镇的镇长西蒙。
		OpenMap("map\\maps106.bmp","map\\map106a.map","map\\map106b.npc");//调入地图
		current.x=106;
		current.y=138;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		azimuth= AZIMUTH_UP;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		vopen();
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
		st_progress=0;
	}
	else if(st_dialog==20)
	{//不论跟谁说话，最后的st_dialog都要变成20，以落入此处，方能恢复走动。
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;//没有接续2000的程序段，使程序在这里等待。
	}
	else if(st_dialog==21)
	{//向大港镇的村民打听去镇长的道路
		Dialog(4,0,0,0,69);
		st_dialog=20;
	}

////////////////////////////////////////////////////////////////////////////////
	else if(st_dialog==30)//以下部分是跟哈德尔的对话
	{
		Dialog(4,0,31,0,65);
		st_dialog=31;
	}
	else if(st_dialog==31)
	{
		Dialog(4,11,0,0,66);
		st_dialog=32;
	}
	else if(st_dialog==32)
	{
		Dialog(4,0,31,0,67);
		st_dialog=20;
	}
////////////////////////////////////////////////////////////////////////////////
	else if(st_dialog==100)//以下部分开始听德里安的报告
	{
		lclose();
		m_info_st=9;//对话态
		m_oldinfo_st=9;
		PlayMidi("midi\\p1011.mid");
		OpenMap("map\\maps103.bmp","map\\map103b.map","map\\map103c.npc");//调入地图
		current.x=95;
		current.y=120;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		lopen();
		CDC *pdc=GetDC();
		current.x--;
		for(i=0;i<12;i++){//镜头向左移动12格
			movest=16;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(30);
			movest=15;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(30);
			movest=14;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(30);
			movest=13;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(30);
			current.x-=1;
		}
		current.x+=2;
		GameSleep(2000);
		for(i=0;i<12;i++){//镜头向右移动12格
			movest=13;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(30);
			movest=14;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(30);
			movest=15;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(30);
			movest=16;
			DrawMap();//绘制地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			GameSleep(30);
			current.x+=1;
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,16,0,0,71);
		st_dialog=101;
	}
	else if(st_dialog==101)
	{
		Dialog(4,0,6,0,72);
		st_dialog=102;
	}
	else if(st_dialog==102)
	{
		Dialog(4,16,0,0,73);
		st_dialog=103;
	}
	else if(st_dialog==103)
	{
		Dialog(4,0,6,0,74);
		st_dialog=104;
	}
	else if(st_dialog==104)
	{
		Dialog(4,16,0,0,75);
		st_dialog=105;
	}
	else if(st_dialog==105)
	{
		Dialog(4,0,6,0,76);
		st_dialog=106;
	}
	else if(st_dialog==106)
	{
		Dialog(4,16,0,0,77);
		st_dialog=107;
	}
	else if(st_dialog==107)
	{
		Dialog(4,16,0,0,78);
		st_dialog=108;
	}
	else if(st_dialog==108)
	{
		Dialog(4,0,6,0,79);
		st_dialog=109;
	}
	else if(st_dialog==109)
	{
		Dialog(4,16,0,0,80);
		st_dialog=110;
	}
	else if(st_dialog==110)
	{
		Dialog(4,0,6,0,81);
		st_dialog=111;
	}
	else if(st_dialog==111)
	{
		Dialog(4,16,0,0,82);
		st_dialog=112;
	}
	else if(st_dialog==112)
	{
		Dialog(4,0,6,0,83);
		st_dialog=113;
	}
	else if(st_dialog==113)
	{
		Dialog(4,16,0,0,84);
		st_dialog=114;
	}
	else if(st_dialog==114)
	{//德里安走
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_1);//老头
		BuffDC.BitBlt(384,328,32,64,&MapDC,448,328,SRCCOPY);//在背景上擦去德里安
		pdc->BitBlt(384,328,32,64,&BuffDC,448,328,SRCCOPY);//显示
		resetblt();
		for(i=0;i<3;i++){//移动
			blt(384,(376+(i*16)),32,48,160,0,64,0,pdc);
			GameSleep(50);
			blt(384,(384+(i*16)),32,48,96,0,32,0,pdc);
			GameSleep(50);
		}
		for(i=0;i<28;i++){//移动
			blt((384+(i*16)),432,32,48,160,144,64,144,pdc);
			GameSleep(50);
			blt((392+(i*16)),432,32,48,128,144,32,144,pdc);
			GameSleep(50);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,0,0,85);
		st_dialog=120;
	}
	else if(st_dialog==120)
	{//卡诺进来
		PlayMidi("midi\\p1010.mid");
		MemDC.SelectObject(hbmp_3);//卡诺
		CDC *pdc=GetDC();
		resetblt();
		for(i=0;i<25;i++){//移动
			blt((808-(i*16)),432,32,48,160,48,64,48,pdc);
			GameSleep(50);
			blt((800-(i*16)),432,32,48,128,48,32,48,pdc);
			GameSleep(50);
		}
		for(i=0;i<6;i++){//移动
			blt(416,(400-(i*16)),32,48,160,96,64,96,pdc);
			GameSleep(50);
			blt(416,(392-(i*16)),32,48,96,96,32,96,pdc);
			GameSleep(50);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,31,0,86);
		st_dialog=121;
	}
	else if(st_dialog==121)
	{
		Dialog(4,0,6,0,87);
		st_dialog=122;
	}
	else if(st_dialog==122)
	{
		Dialog(4,0,31,0,88);
		st_dialog=123;
	}
	else if(st_dialog==123)
	{
		Dialog(4,0,6,0,89);
		st_dialog=124;
	}
	else if(st_dialog==124)
	{
		Dialog(4,0,31,0,90);
		st_dialog=125;
	}
	else if(st_dialog==125)
	{
		Dialog(4,0,6,0,91);
		st_dialog=126;
	}
	else if(st_dialog==126)
	{
		Dialog(4,0,31,0,92);
		st_dialog=127;
	}
	else if(st_dialog==127)
	{
		Dialog(4,0,6,0,93);
		st_dialog=128;
	}
	else if(st_dialog==128)
	{
		Dialog(4,0,31,0,94);
		st_dialog=129;
	}
	else if(st_dialog==129)
	{
		Dialog(4,0,6,0,95);
		st_dialog=130;
	}
	else if(st_dialog==130)
	{
		Dialog(4,0,31,0,96);
		st_dialog=131;
	}
	else if(st_dialog==131)
	{
		Dialog(4,0,6,0,97);
		st_dialog=132;
	}
	else if(st_dialog==132)
	{
		Dialog(4,0,6,0,98);
		st_dialog=133;
	}
	else if(st_dialog==133)
	{
		Dialog(4,0,6,0,99);
		st_dialog=134;
	}
	else if(st_dialog==134)
	{
		Dialog(4,0,6,0,100);
		st_dialog=135;
	}
	else if(st_dialog==135)
	{
		Dialog(4,0,31,0,101);
		st_dialog=136;
	}
	else if(st_dialog==136)
	{
		Dialog(4,0,6,0,102);
		st_dialog=137;
	}
	else if(st_dialog==137)
	{
		Dialog(4,0,31,0,103);
		st_dialog=138;
	}
	else if(st_dialog==138)
	{
		Dialog(4,0,6,0,104);
		st_dialog=139;
	}
	else if(st_dialog==139)
	{
		Dialog(4,0,31,0,105);
		st_dialog=140;
	}
	else if(st_dialog==140)
	{
		Dialog(4,0,6,0,106);
		st_dialog=141;
	}
	else if(st_dialog==141)
	{
		Dialog(4,0,31,0,107);
		st_dialog=142;
	}
	else if(st_dialog==142)
	{
		Dialog(4,0,6,0,108);
		st_dialog=143;
	}
	else if(st_dialog==143)
	{
		Dialog(4,0,31,0,109);
		st_dialog=144;
	}
	else if(st_dialog==144)
	{
		Dialog(3,0,31,0,110);
		st_dialog=145;
	}
	else if(st_dialog==145)
	{//荻娜走过来
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_0);//荻娜
		resetblt();
		for(i=0;i<8;i++){//移动
			blt(416,(480-(i*16)),32,48,160,96,64,96,pdc);
			GameSleep(50);
			blt(416,(472-(i*16)),32,48,128,96,32,96,pdc);
			GameSleep(50);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,26,0,0,111);
		st_dialog=146;
	}
	else if(st_dialog==146)
	{//卡诺转过身来
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_3);//卡诺
		oldlx=416;
		oldly=312;
		oldx=32;
		oldy=48;
		blt(416,312,32,48,96,0,0,0,pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,31,0,112);
		st_dialog=147;
	}
	else if(st_dialog==147)
	{
		Dialog(4,0,6,0,113);
		st_dialog=148;
	}
	else if(st_dialog==148)
	{
		Dialog(4,26,0,0,114);
		st_dialog=149;
	}
	else if(st_dialog==149)
	{
		Dialog(4,0,6,0,115);
		st_dialog=150;
	}
	else if(st_dialog==150)
	{
		Dialog(4,0,31,0,105);
		st_dialog=151;
	}
	else if(st_dialog==151)
	{
		Dialog(4,21,0,0,116);
		st_dialog=152;
	}
	else if(st_dialog==152)
	{
		Dialog(4,0,31,0,117);
		st_dialog=153;
	}
	else if(st_dialog==153)
	{
		Dialog(4,0,6,0,118);
		st_dialog=154;
	}
	else if(st_dialog==154)
	{
		Dialog(4,0,31,0,119);
		st_dialog=155;
	}
	else if(st_dialog==155)
	{
		Dialog(4,26,0,0,120);
		st_dialog=156;
	}
	else if(st_dialog==156)
	{//荻娜走
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_0);//荻娜
		oldlx=416;
		oldly=360;
		oldx=32;
		oldy=48;
		for(i=0;i<3;i++){//移动
			blt(416,(376+(i*16)),32,48,160,0,64,0,pdc);
			GameSleep(50);
			blt(416,(384+(i*16)),32,48,96,0,32,0,pdc);
			GameSleep(50);
		}
		for(i=0;i<26;i++){//移动
			blt((416+(i*16)),432,32,48,160,144,64,144,pdc);
			GameSleep(50);
			blt((424+(i*16)),432,32,48,128,144,32,144,pdc);
			GameSleep(50);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,31,0,121);
		st_dialog=157;
	}
	else if(st_dialog==157)
	{
		Dialog(4,0,6,0,122);
		st_dialog=158;
	}
	else if(st_dialog==158)
	{
		Dialog(4,0,31,0,123);
		st_dialog=159;
	}
	else if(st_dialog==159)
	{
		Dialog(4,0,31,0,124);
		st_dialog=160;
	}
	else if(st_dialog==160)
	{//得到地图
		PlayWave("snd//prop.wav");
		Dialog(1,0,0,3,125);
		GameSleep(500);
		CDC* pdc=GetDC();
		RenewInfoButtons(pdc);//强制刷新
		ReleaseDC(pdc);
		st_dialog=161;
	}
	else if(st_dialog==161)
	{//地图使用说明
		Dialog(1,0,0,0,126);
		st_dialog=162;
	}
	else if(st_dialog==162)
	{
		Dialog(4,0,6,0,127);
		st_dialog=163;
	}
	else if(st_dialog==163)
	{
		Dialog(4,0,31,0,128);
		st_dialog=164;
	}
	else if(st_dialog==164)
	{
		Dialog(4,0,6,0,129);
		st_dialog=165;
	}
	else if(st_dialog==165)
	{//转身
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_3);//卡诺
		oldlx=416;
		oldly=312;
		oldx=32;
		oldy=48;
		blt(416,312,32,48,96,96,0,96,pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,31,0,130);
		st_dialog=166;
	}
	else if(st_dialog==166)
	{
		Dialog(4,0,6,0,131);
		st_dialog=167;
	}
	else if(st_dialog==167)
	{//得到书信
		CDC *pdc=GetDC();
		PlayWave("snd//prop.wav");
		m_info_prop1=3;
		RenewInfo(pdc);//在信息栏中显示物品小图
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(1,0,0,1,132);
		GameSleep(500);
		st_dialog=168;
	}
	else if(st_dialog==168)
	{
		Dialog(4,0,6,0,133);
		st_dialog=169;
	}
	else if(st_dialog==169)
	{
		Dialog(4,0,31,0,134);
		st_dialog=170;
	}
	else if(st_dialog==170)
	{
		vclose();
		st_dialog=171;
		Dialog(5,0,0,0,137);//提示存盘
	}
	else if(st_dialog==171){
		st=4;//第三段
		st_sub1=0;
		st_dialog=0;
		Dialog(2,0,0,0,136);
		Para3Init();
	}
////////////////////////////////////////////////////////////////////////////////
}//第三部分结束
////////////////////////////////////////////////////////////////////////////////
}
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para2Accident(int type)
{
	int i;

m_info_st=9;//对话态
//m_oldinfo_st=9;
if(st_sub1==0){
	//以下部分是西镇的人物
	if(type==161){//村民1
		Dialog(4,0,0,0,15);
		st_dialog=20;
	}
	else if(type==162){//村民2
		Dialog(4,0,0,0,16);
		st_dialog=20;
	}
	else if(type==163){//村民3
		Dialog(4,0,0,0,17);
		st_dialog=20;
	}
	else if(type==164){//村民4
		Dialog(4,0,0,0,18);
		st_dialog=20;
	}
	else if(type==165){//村民5
		Dialog(4,0,0,0,19);
		st_dialog=20;
	}
	else if(type==166){//村民6
		Dialog(4,0,0,0,20);
		st_dialog=20;
	}
	else if(type==167){//镇长
		Dialog(4,0,1,0,14);
		st_dialog=20;
	}
	else if(type==168){//不说话的士兵
		Dialog(4,0,0,0,141);
		st_dialog=20;
	}
	else if(type==169){//卧虫床前
		Dialog(4,0,31,0,56);
		st_dialog=20;
	}
	else if(type==201){//地界A
		Dialog(2,0,0,0,49);
		st_dialog=20;
	}//西镇的人物结束
	//以下部分是大港镇的人物
////////////////////////////////////////////////////////////////////////////////
	else if(type==172)//遇到荻娜
	{//对话完成后，将st_sub1更改为1，此处很重要。
		Dialog(4,0,31,0,24);
		st_dialog=30;
	}
////////////////////////////////////////////////////////////////////////////////
	else if(type==173){//向村民打听道路
		Dialog(4,0,0,0,22);
		st_dialog=21;
	}
	else if(type==176){//镇长办公室门口的卫兵
		Dialog(4,0,0,0,139);
		st_dialog=20;
	}
	else if(type==207){//地界G
		Dialog(2,0,0,0,49);
		st_dialog=20;
	}
	else if(type==206){//地界F
		Dialog(2,0,0,0,49);
		st_dialog=20;
	}//大港镇的人物结束
	//以下部分处理地图切换
	else if(type==202){//地界B（地图106右下方的道路）对应地界C
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map107a.map","map\\map107a.npc");//调入地图
		current.x-=165;
		current.y=114;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==203){//地界C（地图107左下方的道路）对应地界B
		lclose();
		StopActTimer();
		OpenMap("map\\maps106.bmp","map\\map106a.map","map\\map106a.npc");//调入地图
		current.x+=165;
		current.y=114;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==204){//地界D（地图107正下方的道路）对应地界E
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map103a.map","map\\map103a.npc");//调入地图
		current.x=70;
		current.y-=170;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==205){//地界E（地图103正上方的道路）对应地界D
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map107a.map","map\\map107a.npc");//调入地图
		current.x=70;
		current.y+=170;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
}//st_sub1==0的部分结束
////////////////////////////////////////////////////////////////////////////////
else if(st_sub1==1){//响应第二段的地图事件
	if(type==173){//村民
		Dialog(4,22,0,0,144);
		st_dialog=20;
	}
	else if(type==172){//诊所
		if(m_info_prop1==6){//采到药了
			PlayMidi("midi\\p1009.mid");
			m_info_st=10;
			m_oldinfo_st=10;
			Dialog(4,11,0,0,47);
			st_dialog=30;
		}
		else{//未采药
			Dialog(4,22,0,0,144);
			st_dialog=20;
		}
	}
	else if(type==176){//镇长办公室门口的卫兵
		Dialog(4,0,0,0,139);
		st_dialog=20;
	}
	else if(type==205){//地界E
		Dialog(4,22,0,0,43);
		st_dialog=20;
	}
	else if(type==207){//地界G
		Dialog(4,22,0,0,43);
		st_dialog=20;
	}
	else if(type==206){//地界F（切换NPC矩阵上山）
		if(m_info_prop1==6){//采到药了
			Dialog(4,21,0,0,46);
			st_dialog=20;
		}
		else{
			lclose();
			StopActTimer();
			OpenMap("map\\maps103.bmp","map\\map103b.map","map\\map103b.npc");//调入地图
			current.y-=3;
			oldcurrent.x=current.x;
			oldcurrent.y=current.y;
			npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
			DrawMap();//准备地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			DrawActor(1);
			lopen();
			m_info_st=10;//自由战斗态
			m_oldinfo_st=10;
			blood=200;//血
			level=20;//等级
			score=0;//分数
			st_progress=0;
			SetActTimer();
		}
	}
	//下面处理山上的部分
	else if(type==209){//采花的位置（地界I）
		CDC *pdc=GetDC();
		if(st_progress==1){
			Dialog(4,21,0,0,46);
			st_dialog=20;
		}
		else if(st_progress==0){
			StopActTimer();
			MemDC.SelectObject(hbmp_0);//荻娜
			resetblt();
			for(i=0;i<6;i++){//移动
				blt(384,(272+(i*16)),32,48,160,0,64,0,pdc);
				GameSleep(50);
				blt(384,(280+(i*16)),32,48,128,0,32,0,pdc);
				GameSleep(100);
			}
			for(i=0;i<6;i++){//移动
				blt(384,(336-(i*16)),32,48,160,96,64,96,pdc);
				GameSleep(50);
				blt(384,(320-(i*16)),32,48,128,96,32,96,pdc);
				GameSleep(100);
			}
			PlayWave("snd//prop.wav");
			GameSleep(500);
			Dialog(4,21,0,2,46);
			m_info_prop1=6;
			RenewInfo(pdc);//在信息栏中显示物品小图
			st_progress=1;
			//刷新敌群
			m_info_st=10;//自由战斗态
			m_oldinfo_st=10;
			OpenMap("map\\maps103.bmp","map\\map103b.map","map\\map103b.npc");//调入地图
			st_dialog=20;
			ReleaseDC(pdc);
			pdc = NULL;
		}
	}
	else if(type==208){//下山的位置（地界H）
		if(st_progress==1){//采到药才允许下山
			lclose();
			StopActTimer();
			OpenMap("map\\maps103.bmp","map\\map103b.map","map\\map103a.npc");//调入地图
			current.y+=3;
			oldcurrent.x=current.x;
			oldcurrent.y=current.y;
			npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
			DrawMap();//准备地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			DrawActor(1);
			lopen();
			m_info_st=8;//自由态
			m_oldinfo_st=8;
			SetActTimer();
		}
		else if(st_progress==0){
			Dialog(4,22,0,0,45);
			st_dialog=20;
		}
	}
}////st_sub1==0的部分结束
else if(st_sub1==2){//响应第三段的地图事件
	if(type==171){//村民
		Dialog(4,0,0,0,138);
		st_dialog=20;
	}
	else if(type==170){//镇长
		Dialog(4,0,1,0,62);
		st_dialog=20;
	}
	else if(type==168){//不说话的士兵
		Dialog(4,0,0,0,141);
		st_dialog=20;
	}
	else if(type==201){//地界A
		Dialog(2,0,0,0,49);
		st_dialog=20;
	}//西镇的人物结束
	//以下部分是大港镇的人物
////////////////////////////////////////////////////////////////////////////////
	else if(type==176){//镇长办公室门口的卫兵（进入下一段的入口）
		Dialog(4,0,0,0,70);//（从这里开始听德里安的报告）
		st_dialog=100;
	}
////////////////////////////////////////////////////////////////////////////////
	else if(type==175){//向村民打听道路
		Dialog(4,0,31,0,68);
		st_dialog=21;
	}
	else if(type==178){//港口的士兵
		Dialog(4,0,0,0,140);
		st_dialog=20;
	}
	else if(type==174)//遇到哈德尔
	{//对话完成后，将st_sub1更改为1，此处很重要。
		Dialog(4,11,0,0,64);
		st_dialog=30;
	}
	else if(type==207){//地界G
		Dialog(2,0,0,0,49);
		st_dialog=20;
	}
	else if(type==206){//地界F
		Dialog(2,0,0,0,49);
		st_dialog=20;
	}//大港镇的人物结束
////////////////////////////////////////////////////////////////////////////////
	//以下是地图的切换段
	else if(type==202){//地界B（地图106右下方的道路）对应地界C
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map107a.map","map\\map107a.npc");//调入地图
		current.x-=165;
		current.y=114;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==203){//地界C（地图107左下方的道路）对应地界B
		lclose();
		StopActTimer();
		OpenMap("map\\maps106.bmp","map\\map106a.map","map\\map106b.npc");//调入地图
		current.x+=165;
		current.y=114;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==204){//地界D（地图107正下方的道路）对应地界E
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map103b.map","map\\map103c.npc");//调入地图
		current.x=70;
		current.y-=170;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==205){//地界E（地图103正上方的道路）对应地界D
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map107a.map","map\\map107a.npc");//调入地图
		current.x=70;
		current.y+=170;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
}////st_sub1==2的部分结束
////////////////////////////////////////////////////////////////////////////////
}
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para2Timer()
{
	processenemies();
}
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████